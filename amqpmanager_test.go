package amqpmanager_test

import (
	"bitbucket.org/jrlangford/amqpmanager"
	"bitbucket.org/jrlangford/amqpmanager/mocks"
	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"testing"
)

// TODO: Add some negative tests

var amqpConf = amqpmanager.AMQPConf{}

func TestInitializationAndCleanup(t *testing.T) {
	logger := logrus.New()

	cl := amqpmanager.AMQP{}

	err := cl.Init(amqpConf, logger)
	if err != nil {
		t.Fatalf("Init failed: %s", err)
	}

	err = cl.CleanUp()
	if err != nil {
		t.Fatalf("CleanUp failed: %s", err)
	}
}

func TestRunAndStop(t *testing.T) {
	logger := logrus.New()

	// Creates a mock AMQPDialer instance
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockAMQPDialer := mocks.NewMockDialer(mockCtrl)

	var cl amqpmanager.AMQP
	cl.Dialer = mockAMQPDialer

	err := cl.Init(amqpConf, logger)
	if err != nil {
		t.Fatalf("Init failed: %s", err)
	}

	mockAMQPDialer.EXPECT().Dial(cl.ReadStringConf()).Return(&amqpmanager.AMQPConnection{}, nil).Times(1)

	err = cl.Run()
	if err != nil {
		t.Fatalf("Run failed: %s", err)
	}

	err = cl.CleanUp()
	if err != nil {
		t.Fatalf("CleanUp failed: %s", err)
	}
}
