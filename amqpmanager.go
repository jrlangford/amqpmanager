// Package amqpmanager provides methods to easily manage amqp clients
package amqpmanager

import (
	"fmt"
	gli "github.com/jrlangford/go-logger-interface"
	"github.com/streadway/amqp"
)

// TODO: Handle server disconnections

// AMQPConf stores the configuration for amqp
type AMQPConf struct {
	Host       string
	Port       int
	User       string
	Password   string
	Driver     string
	stringConf string
}

// AMQPConnector implements methods to adapt amqp.Connection
type AMQPConnection struct {
	*amqp.Connection
}

// Channel adapts amqp.Connection.Channel to return the local Channel interface
func (conn AMQPConnection) Channel() (c Channel, err error) {
	c, err = conn.Connection.Channel()
	return
}

// AMQP Dialer provides a method signature to connect to a AMQP server
type Dialer interface {
	Dial(stringConf string) (conn *AMQPConnection, err error)
}

// AMQPDialer implements methods to handle amqp connections
type AMQPDialer struct {
}

// Open adapts amqp.Dial to make DBConnector Connectable
func (d *AMQPDialer) Dial(stringConf string) (conn *AMQPConnection, err error) {
	conn = &AMQPConnection{}
	conn.Connection, err = amqp.Dial(stringConf)
	return
}

// AMQP holds the state of the client
type AMQP struct {
	Connectable
	Dialer
	conf AMQPConf
	log  gli.LoggerInterface
}

// Init adapts init so AMQP is Runnable
func (cl *AMQP) Init(conf interface{}, log gli.LoggerInterface) (err error) {
	return cl.init(conf.(AMQPConf), log)
}

// Run adapts connect so AMQP is Runnable
func (cl *AMQP) Run() (err error) {
	return cl.Connect()
}

// Stop adapts Disconnect so AMQP is Runnable
func (cl *AMQP) Stop() (err error) {
	return cl.Disconnect()
}

// Cleanup adapts an empty function so AMQP is Runnable
func (cl *AMQP) CleanUp() (err error) {
	return
}

// init defines the configuration strings and interfaces
func (cl *AMQP) init(conf AMQPConf, log gli.LoggerInterface) (err error) {
	cl.log = log
	cl.conf = conf

	cl.conf.stringConf = fmt.Sprintf(
		"%s://%s:%s@%s:%d/",
		conf.Driver,
		conf.User,
		conf.Password,
		conf.Host,
		conf.Port,
	)

	if cl.Dialer == nil {
		cl.Dialer = &AMQPDialer{}
	}
	return
}

func (cl *AMQP) ReadStringConf() string {
	return cl.conf.stringConf
}

// Connect connects to the AMQP server
func (cl *AMQP) Connect() (err error) {
	cl.Connectable, err = cl.Dial(cl.conf.stringConf)
	if err != nil {
		return
	}

	cl.log.Info("AMQP connection successful")
	return
}

// Disconnect closes the database connection
func (cl *AMQP) Disconnect() (err error) {
	err = cl.Close()
	cl.log.Info("AMQP disconnected")
	return
}
