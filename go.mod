module bitbucket.org/jrlangford/amqpmanager

go 1.15

require (
	bitbucket.org/jrlangford/golog v0.0.0-20181113212437-cfd2e7c615ed
	github.com/golang/mock v1.4.4
	github.com/jrlangford/go-logger-interface v0.0.0-20201109075755-1f1a3dc37c19
	github.com/sirupsen/logrus v1.7.0
	github.com/streadway/amqp v1.0.0
)
