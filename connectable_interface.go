//

package amqpmanager

import (
	"crypto/tls"
	"github.com/streadway/amqp"
	"net"
)

// Connectable ...
type Connectable interface {
	/*
	   LocalAddr returns the local TCP peer address, or ":0" (the zero value of net.TCPAddr)
	   as a fallback default value if the underlying transport does not support LocalAddr().
	*/
	LocalAddr() net.Addr
	// ConnectionState returns basic TLS details of the underlying transport.
	// Returns a zero value when the underlying connection does not implement
	// ConnectionState() tls.ConnectionState.
	ConnectionState() tls.ConnectionState
	/*
	   NotifyClose registers a listener for close events either initiated by an error
	   accompanying a connection.close method or by a normal shutdown.

	   On normal shutdowns, the chan will be closed.

	   To reconnect after a transport or protocol error, register a listener here and
	   re-run your setup process.

	*/
	NotifyClose(receiver chan *amqp.Error) chan *amqp.Error
	/*
	   NotifyBlocked registers a listener for RabbitMQ specific TCP flow control
	   method extensions connection.blocked and connection.unblocked.  Flow control is
	   active with a reason when Blocking.Blocked is true.  When a Connection is
	   blocked, all methods will block across all connections until server resources
	   become free again.

	   This optional extension is supported by the server when the
	   "connection.blocked" server capability key is true.

	*/
	NotifyBlocked(receiver chan amqp.Blocking) chan amqp.Blocking
	/*
	   Close requests and waits for the response to close the AMQP connection.

	   It's advisable to use this message when publishing to ensure all kernel buffers
	   have been flushed on the server and client before exiting.

	   An error indicates that server may not have received this request to close but
	   the connection should be treated as closed regardless.

	   After returning from this call, all resources associated with this connection,
	   including the underlying io, Channels, Notify listeners and Channel consumers
	   will also be closed.
	*/
	Close() error
	/*
	   Channel opens a unique, concurrent server channel to process the bulk of AMQP
	   messages.  Any error from methods on this receiver will render the receiver
	   invalid and a new Channel should be opened.

	*/
	Channel() (Channel, error)
}
